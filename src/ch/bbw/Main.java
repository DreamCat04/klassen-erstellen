package ch.bbw;

import ch.bbw.Diamond.Diamond;
import ch.bbw.RealTriangle.RealTriangle;
import ch.bbw.Rectangle.Rectangle;
import ch.bbw.Square.Square;
import ch.bbw.Triangle.Triangle;
import jdk.jshell.execution.JdiDefaultExecutionControl;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Geben Sie die Höhe des Dreiecks ein:");
		int height = sc.nextInt();


		/*Triangle triangle1 = new Triangle();
		triangle1.draw();

		Triangle triangle2 = new Triangle();
		triangle2.setHeight(5);
		triangle2.draw();*/

		Triangle triangle3 = new Triangle();
		triangle3.setHeight(height);
		triangle3.draw();

		System.out.print("Geben Sie die Grösse des Quadrates ein:");

		int size = sc.nextInt();

		Square square = new Square();
		square.setSize(size);
		square.draw();

		System.out.print("Geben Sie die Breite des Rechtecks ein:");
		int width = sc.nextInt();

		System.out.print("Geben Sie die Höhe des Rechtecks ein:");
		int heightRec = sc.nextInt();

		Rectangle rectangle = new Rectangle();
		rectangle.setWidth(width);
		rectangle.setHeight(heightRec);
		rectangle.draw();

		System.out.print("Geben Sie die Grösse des Dreiecks ein: ");
		int realSize = sc.nextInt();

		RealTriangle realTriangle = new RealTriangle();
		realTriangle.setSize(realSize);
		realTriangle.draw();

		System.out.print("Geben Sie die Grösse des Diamanten ein: ");
		int diaSize = sc.nextInt();

		Diamond dia = new Diamond();
		dia.setSize(diaSize);
		dia.draw();

		sc.close();
	}
}
