package ch.bbw.RealTriangle;

public class RealTriangle {
	private int size;

	public void setSize(int size) {
		this.size = size;
	}
	public void draw() {
		StringBuilder triangleChars = new StringBuilder("*");
		String space = " ";
		StringBuilder offset = new StringBuilder();
		StringBuilder output = new StringBuilder("");

		offset.append(" ".repeat(Math.max(0, size)));
		output.append(offset);
		output.append(triangleChars);
		System.out.println(offset.append(triangleChars));
		for(int i = 0; i < size; i++) {
			output.append("**");
			output.delete(0, 1);
			System.out.println(output);
		}

	}
}
