package ch.bbw.Square;

public class Square {
	private int size;

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void draw() {
		String squareW = "";
		for(int i = 0; i < size; i++) {
			squareW+="*";
		}
		for (int i = 0; i < size; i++) {
			System.out.println(squareW);
		}
	}

}
