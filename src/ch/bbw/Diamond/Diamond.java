package ch.bbw.Diamond;

public class Diamond {

	private int size;
	public void setSize(int size) {
		this.size = size;
	}

		public void draw() {
			StringBuilder diamondChars = new StringBuilder("*");
			String space = " ";
			StringBuilder offset = new StringBuilder();
			StringBuilder output = new StringBuilder("");

			offset.append(" ".repeat(Math.max(0, size)));
			output.append(offset);
			output.append(diamondChars);
			System.out.println(offset.append(diamondChars));

			for (int i = 0; i < size; i++) {
				output.append("**");
				output.delete(0, 1);
				System.out.println(output);
			}
			for (int i = 0; i < size; i++) {
				output.delete(size-2, size);
				output.insert(0, space);
				System.out.println(output);
			}
		}
	}
